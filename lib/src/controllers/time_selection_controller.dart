import 'package:get/get.dart';

class TimeSelectionController extends GetxController {
  RxInt? selectedHour;
  RxInt? selectedMinute;
  RxInt? selectedSecond;

  onTimer() {
    
    selectedHour = 0.obs;
    selectedMinute = 0.obs;
    selectedSecond = 0.obs;
  }

  updateHour(int hour) {
    selectedHour?.value = hour;
  }

  updateMinute(int minute) {
    selectedMinute?.value = minute;
  }

  updateSecond(int second) {
    selectedSecond?.value = second;
  }
}
