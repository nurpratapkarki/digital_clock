import 'package:digital_clock/src/controllers/clock_controller.dart';

import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../widgets/change_time_widget.dart';

class ClockScreen extends StatelessWidget {
  ClockScreen({super.key});

  final ClockController _clockController = Get.put(ClockController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: 
      const Text('Digital Clock'),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(
              () => Text(
                _clockController.dateTime
                    .toString()
                    .split(' ')[1]
                    .substring(0, 8),
                style: const TextStyle(fontSize: 48),
              ),
            ),
            const SizedBox(height: 20),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [ChangeTime()]),
          ],
        ),
      ),
    );
  }
}
