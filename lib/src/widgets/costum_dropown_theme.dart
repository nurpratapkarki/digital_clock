import 'package:digital_clock/src/controllers/time_selection_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomDropdown extends StatelessWidget {
  final String label;
  final List<int> values;
  final void Function(int) onChanged;
  final int initialValue;
  final RxInt selectedValue;

   CustomDropdown({
    super.key,
    required this.label,
    required this.values,
    required this.onChanged,
    required this.initialValue,
    required this.selectedValue,
  });
    final TimeSelectionController timeController =
        Get.find<TimeSelectionController>();

  @override
  Widget build(BuildContext context) {

    return DropdownButton<int>(
      hint: Text(label),
      value: initialValue,
      onChanged: (int? value) {
        if (value != null) {
          selectedValue.value =
              value; // Update selected value in the controller
          onChanged(value);
          switch (label) {
            case 'Hours':
              timeController.updateHour(value);
              break;
            case 'Minutes':
              timeController.updateMinute(value);
              break;
            case 'Seconds':
              timeController.updateSecond(value);
              break;
          }
        }
      },
      items: values
          .map((value) => DropdownMenuItem<int>(
                value: value,
                child: Text(value.toString().padLeft(2, '0')),
              ))
          .toList(),
    );
  }
}
