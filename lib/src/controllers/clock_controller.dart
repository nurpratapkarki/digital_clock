import 'package:get/get.dart';
import 'dart:async';

class ClockController extends GetxController {
  final Rx<DateTime> _dateTime = DateTime.now().obs;
  final RxBool _useSystemTime = true.obs;
  late Timer _timer;

  // Getters for the observable variables
  DateTime get dateTime => _dateTime.value;
  bool get useSystemTime => _useSystemTime.value;


   onTimer() {
  
    _startTimer();
  }

  // Method to start the timer for automatic time updates
 _startTimer() {
    _timer = Timer.periodic(
      const Duration(seconds: 1),
      (_) {
        if (useSystemTime) {
          _updateTime();
        } else {
          _updateTimeManually();
        }
      },
    );
  }

  // Method to update time automatically
   _updateTime() {
    _dateTime.value = DateTime.now();
  }

  // Method to update time manually
   _updateTimeManually() {
    DateTime currentDateTime = dateTime;
    DateTime updatedTime = currentDateTime.add(const Duration(seconds: 1));

    // Handle rollover of seconds, minutes, and hours
    int seconds = updatedTime.second;
    int minutes = updatedTime.minute;
    int hours = updatedTime.hour;

    if (seconds == 60) {
      seconds = 0;
      minutes++;
      if (minutes == 60) {
        minutes = 0;
        hours++;
        if (hours == 24) {
          hours = 0; // Reset hours to 00 after 24
        }
      }
    }

    // Update the time in the controller with the adjusted values
    _dateTime.value = DateTime(
      currentDateTime.year,
      currentDateTime.month,
      currentDateTime.day,
      hours,
      minutes,
      seconds,
    );
  }

  // Method to toggle between system time and manual time
   toggleUseSystemTime() {
    _useSystemTime.value = !_useSystemTime.value;

    if (_useSystemTime.value) {
      // If switching to system time, start the timer if it's not active
      if (!_timer.isActive) {
        _startTimer();
      }
    } else {
      // If switching to manual time, do nothing with the timer
      // Timer continues updating time manually
    }
  }

  // Method to manually update the time
   updateTimeManually(DateTime newDateTime) {
    _dateTime.value = newDateTime;
  }

  @override
   onClose() {
    _timer.cancel(); // Cancel the timer when the controller is closed
    super.onClose();
  }
}
