import 'package:digital_clock/src/controllers/clock_controller.dart';
import 'package:digital_clock/src/controllers/time_selection_controller.dart';
import 'package:digital_clock/src/widgets/costum_dropown_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangeTime extends StatelessWidget {
  final ClockController _clockController = Get.find<ClockController>();

  ChangeTime({super.key});

  final TimeSelectionController timeController =
      Get.put(TimeSelectionController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            const Text('Use Manual Time '),
            Obx(() => Switch(
                  value: !_clockController.useSystemTime,
                  onChanged: (newValue) {
                    _clockController.toggleUseSystemTime();
                    // No need to setState in a stateless widget
                  },
                )),
          ],
        ),
        Row(
          children: [
            //Added Obx to Display the changes when Value is Selected

            Obx(
              () => CustomDropdown(
                label: 'Hours',
                values: List.generate(24, (index) => index),
                onChanged: timeController.updateHour,
                initialValue: timeController.selectedHour!.value,
                selectedValue: timeController.selectedHour!,
              ),
            ),
            const SizedBox(width: 10),
            Obx(
              () => CustomDropdown(
                label: 'Minutes',
                values: List.generate(60, (index) => index),
                onChanged: timeController.updateMinute,
                initialValue: timeController.selectedMinute!.value,
                selectedValue: timeController.selectedMinute!,
              ),
            ),
            const SizedBox(width: 10),
            Obx(
              () => CustomDropdown(
                label: 'Seconds',
                values: List.generate(60, (index) => index),
                onChanged: timeController.updateSecond,
                initialValue: timeController.selectedSecond!.value,
                selectedValue: timeController.selectedSecond!,
              ),
            ),
          ],
        ),

        // Update button here

        const SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            _updateTime(
              timeController.selectedHour!.value,
              timeController.selectedMinute!.value,
              timeController.selectedSecond!.value,
            );
          },
          child: const Text('Confirm'),
        ),
      ],
    );
  }

  // updates the time

  void _updateTime(int hour, int minute, int second) {
    _clockController.updateTimeManually(
      DateTime(
        _clockController.dateTime.year,
        _clockController.dateTime.month,
        _clockController.dateTime.day,
        hour,
        minute,
        second,
      ),
    );
  }
}
